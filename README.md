# CondoVive API

CondoVive is an administration platform for condo's administration process. It has multiple functions for administrators, board of directors and residents. Our job is to make administration easy.

## Objective
Your task is to write a RESTful API with the following *requirements*, please read carefuly and follow the instructions to make your evaluation process easier.

## Requirements
**The user must be able to:**

* Condos (name, address)
    * Create a new condo.
    * Get a condo by ID.
* Apartment (name, apartment_number)
    * Create a new apartment inside a previously created condo.
    * Get all the apartments inside a condo.
    * Get an apartment by ID.
    * Update an apartment's name.
    * Delete an apartment
* Resident (name, email, phone)
    * Create a new resident inside an apartment.
    * Get all the residents inside an apartment.
    * Get a resident by ID.
    * Update resident information.
    * Delete a resident.
* Monthly rental fee (amount, date)
    * Create a new monthly fee by apartment.
    * A resident can pay the apartment's monthly fee.
    * An administrator can get a list of all non payed apartment's monthly fees.
    * Keep a log of all the apartments payment history.

## Security requirements
Be careful for what kind of actions an administrator and a resident user can do.
Example: Only an administrator can create condos and apartments.
Set all restrictions for each kind of user.

## Testing
Publish your work using Heroku and share the api URL with us.
Provide a database dump so we can replicate the database locally.

## General specifications
- You are free to use any package, framework or library as long as you can justify their use.
- You can use any database of your choice.
- Follow RESTful best practices.
- Use git and do small and purposeful commits to facilitate the evaluation of progress.
- Include a README.md file with instructions on how to **SETUP** & **USE** your project locally to test it. (This is crucial, remember that if we can't install it and run it easily we are not able to evaluate it).
- Upload your solution to your Github or Bitbucket account and share a link with your evaluator.
- The test has been designed with enough time to do a good job, take your time and watch for quality.
- We know there are a lot of approaches for this exercise, we want your approach to be unique, for that, take into account to unit test your code.

### Time to complete: According to previous agreement
We hope to hear from you soon, enjoy the challenge and do your best.

Good luck!

*Disforma*


